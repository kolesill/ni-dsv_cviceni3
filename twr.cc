#include <grpcpp/grpcpp.h>
#include <string>
#include <thread>
#include <mutex>
#include <vector>
#include <optional>
#include "twr.grpc.pb.h"


using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;

using twr::twrRequest;
using twr::twrResponse;
using twr::Twr;


using namespace std;

enum Direction {
  LEFT, RIGHT
};

class twrNode : public Twr::Service
{
private:
  int nodeId;
  mutex mL;
  mutex mR;
  // stubs
  string neighborAddressL = "";
  string neighborAddressR = "";
  shared_ptr<Twr::Stub> stubL;
  shared_ptr<Twr::Stub> stubR;
  shared_ptr<Channel> channelL;
  shared_ptr<Channel> channelR;
  // skeletons
  string skeletonAddressL = "";
  string skeletonAddressR = "";
  thread *tSkeletonL = NULL;
  thread *tSkeletonR = NULL;
  shared_ptr<Server> skeletonL;
  shared_ptr<Server> skeletonR;
  bool readyL = false;
  bool readyR = false;

public:
  twrNode(int nodeId = 0, string skeletonAddressL = "", string skeletonAddressR = "", string neighborAddressL = "", string neighborAddressR = "")
  {
    this->nodeId = nodeId;
    this->skeletonAddressL = skeletonAddressL;
    this->skeletonAddressR = skeletonAddressR;
    this->neighborAddressL = neighborAddressL;
    this->neighborAddressR = neighborAddressR;
    cout << "------------------" << endl;
    cout << "NodeID: " << nodeId << endl;
    cout << "SkeletonL: " << skeletonAddressL << " R: " << skeletonAddressR << endl;
    cout << "NeighborL: " << neighborAddressL << " R: " << neighborAddressR << endl;
  }

  // send messag to neighbor stub
  void sendMessage(int receiverId, int senderId, string content, Direction direction)
  {
    if (direction == LEFT) {
      mL.lock();
    } else {
      mR.lock();
    }
    // assemble request
    twrRequest request;
    request.set_receiverid(receiverId);
    request.set_senderid(senderId);
    request.set_content(content);
    // container for server response
    twrResponse reply;
    // Context can be used to send meta data to server or modify RPC behaviour
    ClientContext context;
    // Actual Remote Procedure Call
    optional<Status> status;
    if (direction == LEFT) {
      status = stubL->sendMessage(&context, request, &reply);
    } else {
      status = stubR->sendMessage(&context, request, &reply);
    }
    // Returns results based on RPC status
    if (status->ok()) {
    } else {
      cout << status->error_code() << ": " << status->error_message() << endl;
    }

    if (direction == LEFT) {
      mL.unlock();
    } else {
      mR.unlock();
    }
  }

  // receives message from neighbor stub
  Status sendMessage(ServerContext* context, const twrRequest* request, twrResponse* reply) override
  {
    // message is for this node
    if (request->receiverid() == nodeId) {
      cout << nodeId << " got message from " << request->senderid() << " \"" << request->content() << "\"" << endl;
      // delivered
      reply->set_code(0);
    } else {
      Direction direction = request->content() == "LEFT" ? LEFT : RIGHT;
      cout << nodeId << ": sender >> " << request->senderid() << " sends mesage to " << request->receiverid() << " \"" << request->content() << "\"" << endl;
      sendMessage(request->receiverid(),request->senderid(),request->content(), direction);
      reply->set_code(1);
    }
    // message processed
    return Status::OK;
  }

  void injectMessage(int receiverId, string content, Direction direction)
  {
    sendMessage(receiverId, nodeId, content, direction);
  }

  void createSkeleton(Direction direction)
  {
    shared_ptr<Server> skeleton = direction == LEFT ? this->skeletonL : this->skeletonR;
    string server_address(direction == LEFT ? this->skeletonAddressL : this->skeletonAddressR);
    ServerBuilder builder;
    // Listen on the given address without any authentication mechanism
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    // Register "service" as the instance through which
    // communication with client takes place
    builder.RegisterService(this);
    // Assembling the skeleton interface
    skeleton = builder.BuildAndStart();
    cout << "Server listening on port: " << server_address << endl;
    // server started;
    if (direction == LEFT) {
      this->readyL = true;
    } else {
      this->readyR = true;
    }

    skeleton->Wait();
  }

  // creating a single thread serving as skeleton
  void startSkeletons()
  {
    this->tSkeletonL = new thread(&twrNode::createSkeleton, this, LEFT);
    this->tSkeletonR = new thread(&twrNode::createSkeleton, this, RIGHT);
    // wait until the thread is ready
    while (this->readyL != true || this->readyR != true) {}
    cout << "Skeletons " << nodeId << " are ready." << endl;
  }

  void startStubs()
  {
    this->channelL = grpc::CreateChannel(this->neighborAddressL, grpc::InsecureChannelCredentials());
    this->stubL = Twr::NewStub(this->channelL);
    this->channelR =  grpc::CreateChannel(this->neighborAddressR, grpc::InsecureChannelCredentials());
    this->stubR = Twr::NewStub(this->channelR);
  }
};

int main(int argc, char* argv[])
{
  vector<shared_ptr<twrNode>> ring;
  // creating nodes
  ring.emplace_back(make_shared<twrNode>(1, "127.0.0.1:60001", "127.0.0.1:60011", "127.0.0.1:60012", "127.0.0.1:60006"));
  ring.emplace_back(make_shared<twrNode>(2, "127.0.0.1:60002", "127.0.0.1:60012", "127.0.0.1:60013", "127.0.0.1:60001"));
  ring.emplace_back(make_shared<twrNode>(3, "127.0.0.1:60003", "127.0.0.1:60013", "127.0.0.1:60014", "127.0.0.1:60002"));
  ring.emplace_back(make_shared<twrNode>(4, "127.0.0.1:60004", "127.0.0.1:60014", "127.0.0.1:60015", "127.0.0.1:60003"));
  ring.emplace_back(make_shared<twrNode>(5, "127.0.0.1:60005", "127.0.0.1:60015", "127.0.0.1:60016", "127.0.0.1:60004"));
  ring.emplace_back(make_shared<twrNode>(6, "127.0.0.1:60006", "127.0.0.1:60016", "127.0.0.1:60011", "127.0.0.1:60005"));
  // starting skeletons
  for (auto node : ring) node->startSkeletons();
  // starting stubs
  for (auto node : ring) node->startStubs();
  // sending one message over the entire ring
  ring[0]->injectMessage(1, "LEFT", LEFT);
  ring[0]->injectMessage(1, "RIGHT", RIGHT);
  // wait for keypress
  int x;
  cin >> x;
  return 0;
}
